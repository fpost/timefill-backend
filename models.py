from database import db
import datetime


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(64))
    start = db.Column(db.DateTime, default=datetime.datetime.now(), nullable=False)
    end = db.Column(db.DateTime, default=None, nullable=True)


