from flask import Flask
from flask_restplus import Resource, Api, reqparse
from database import db

from models import Task as TaskModel
from datetime import datetime


def create_app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/task.db'
    db.init_app(app)
    return app


app = create_app()
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('category', type=str, help='Must be: sleep, work, personal, moving, noting, study, other')


@api.route('/task')
class Task(Resource):
    def get(self):
        tasks = []
        for task in db.session.query(TaskModel).all():
            tasks.append({
                "id": task.id,
                "category": task.category,
                "start": datetime.timestamp(task.start),
                "end": datetime.timestamp(task.end)
            })
        return tasks

    @api.expect(parser)
    def post(self):
        args = parser.parse_args(strict=True)
        task = TaskModel(
            category=args['category']
        )
        db.session.add(task)
        db.session.commit()
        return {
            "status": "SUCCES"
        }


@api.route('/task/active')
class ActiveTask(Resource):
    def get(self):
        task = db.session.query(TaskModel).filter_by(end=None).first()
        if task is not None:
            return {
                "id": task.id,
                "category": task.category,
                "start": datetime.timestamp(task.start),
                "end": None
            }
        return None

    def patch(self):
        task = db.session.query(TaskModel).filter_by(end=None).first()
        if task is None:
            return None
        task_id = task.id
        task.end = datetime.now()
        db.session.commit()
        task = db.session.query(TaskModel).get(task_id)
        return {
            "id": task.id,
            "category": task.category,
            "start": datetime.timestamp(task.start),
            "end": datetime.timestamp(task.end)
        }


with app.app_context():
    db.create_all()

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
